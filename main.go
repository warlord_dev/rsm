package main

import (
	generator "bitbucket.org/renegatum/rsm/generator"
)

func main() {
	generator.RunGenerator()
}
