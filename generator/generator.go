package generator

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

var (
	foldersNames []string = []string{
		"router",
		"test",
		"services",
		"controllers",
		"models",
		"conf",
	}
	gitignore string = `
.vscode 
.idea
.bin
*.exe
*.dll
*.so
*.dylib
*.out
.glide/
.DS_Store
*.swp
*.swo `
)

func RunGenerator() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("\x1b[31;1m Welcome, RenegatumSoul Microservices generator! \x1b[0m")
	fmt.Println("\x1b[31;1m your app name? \x1b[0m")
	scanner.Scan()
	text := scanner.Text()
	fmt.Println("\x1b[31;1m need some view? (y/n) \x1b[0m")
	scanner.Scan()
	text2 := scanner.Text()
	if text2 == "yes" || text2 == "y" {
		foldersNames = append(foldersNames, "views")
	}
	generateApp(text)
	fmt.Println("\x1b[31;1m generate gitignore? (y/n) \x1b[0m")
	scanner.Scan()
	text3 := scanner.Text()
	if text3 == "yes" || text3 == "y" {
		generateGitIgnore(text)
	}
	ok()
}

func check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func ok() {
	fmt.Println("ok it ready to work")
}

func generateGitIgnore(path string) {
	_, _ = os.OpenFile(path+"/.gitignore", os.O_RDONLY|os.O_CREATE, 0666)
	ioutil.WriteFile(path+"/.gitignore", []byte(gitignore), 0666)
}

//generateApp - Generate folders of yours app
func generateApp(name string) {
	folder := fmt.Sprintf("/%v", name)
	os.Mkdir(folder, os.ModePerm)
	for _, folderName := range foldersNames {
		generateMicroServiceFolder(name, folderName)
	}
}

func generateMicroServiceFolder(topPath string, folderName string) {
	folder := fmt.Sprintf("%v/%v", topPath, folderName)
	os.MkdirAll(folder, os.ModePerm)
	fmt.Println(fmt.Sprintf("generate %s folder", folderName))
}
